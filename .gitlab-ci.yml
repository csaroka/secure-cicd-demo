
stages:
  - infrastructure-setup
  - infrastructure-plan
  - infrastructure-apply
  - setup-pipeline
  - test
  - build
  - security-review
  - development
  - QA
  - prod

variables:
  IMAGE_DETAILS: image-details.txt
  TF_ROOT: ${CI_PROJECT_DIR}/terraform
  TERRAFORM_IMAGE_VERSION: 0.12.28

build-kritis-image:
  stage: setup-pipeline
  image: docker:19.03.8
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    FULL_IMAGE: "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/kritis-signer"
  services:
    - docker:19.03.8-dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - cd build-images/
    - docker build -t ${FULL_IMAGE}:latest -t ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA} .
    - docker push ${FULL_IMAGE}
  only:
    - tags

dockerfile-lint:
  stage: test
  variables:
    HADOLINT_VERSION: "1.18.0"
  before_script:
  - wget -O hadolint https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64
  - chmod +x hadolint
  script:
  - ./hadolint --config security/hadolint.yaml Dockerfile

secret-scanner:
  stage: test
  extends:
    - .golang-image
  before_script:
    - go get -u github.com/ezekg/git-hound
  script:
    - git-hound -config security/githound.yaml sniff > output.txt
    - cat output.txt

test-coverage-app:
  stage: test
  extends:
    - .golang-image
  before_script:
    - go get -u github.com/jstemmer/go-junit-report
    - go get -u github.com/t-yuki/gocover-cobertura
  script:
    - cd src
    - mkdir -p reports
    # Build the app for compliation
    - go build -o ${CI_PROJECT_DIR}/src/target/hello-world . # run build
    # Run tests & coverage
    - go test -v -coverprofile reports/coverage.out > reports/test-results.out
    # JUnit Test Output Conversion
    - cat reports/test-results.out | go-junit-report > reports/test-report.xml # Run tests, collect coverage output to junit format
    # Generate Coverage Output
    - go tool cover -func=reports/coverage.out # display stdout
    - go tool cover -html=reports/coverage.out -o reports/coverage.html # convert to HTML
    # Convert to Cobertura for CI logs
    - gocover-cobertura < reports/coverage.out > reports/coverage-report.xml

    # replace the pathing for cobertura report output
    - export NEW_CI_DIR=$(echo ${CI_PROJECT_DIR} | sed 's/\//\\\//g')
  artifacts:
    expire_in: 30 minutes
    paths:
      - ${CI_PROJECT_DIR}/src/target/hello-world
      - ${CI_PROJECT_DIR}/src/reports/coverage.html
      - ${CI_PROJECT_DIR}/src/reports/test-report.xml
      - ${CI_PROJECT_DIR}/src/reports/coverage.out
      - ${CI_PROJECT_DIR}/src/reports/test-results.out
      - ${CI_PROJECT_DIR}/src/reports/coverage-report.xml
    reports:
        junit: ${CI_PROJECT_DIR}/src/reports/test-report.xml
        cobertura: ${CI_PROJECT_DIR}/src/reports/coverage-report.xml

validate-licenses:
  stage: test
  extends:
    - .golang-image
  before_script:
    - wget https://github.com/mitchellh/golicense/releases/download/v0.2.0/golicense_0.2.0_linux_x86_64.tar.gz
    - tar -xzf golicense_0.2.0_linux_x86_64.tar.gz
    - mv golicense /usr/local/bin/golicense
  script:
    - cd src
    - mkdir -p ${CI_PROJECT_DIR}/reports
    # Build the app for compliation
    - go build -o hello-world-license .
    - golicense -plain -out-xlsx ${CI_PROJECT_DIR}/reports/license-report.xlsx ../security/license-policy.hcl hello-world-license
  artifacts:
    expire_in: 1 day
    paths:
      - ${CI_PROJECT_DIR}/reports/license-report.xlsx

go-linter:
  stage: test
  extends:
    - .golang-image
  before_script:
    - go get -u golang.org/x/lint/golint
  script:
    - cd src/
    - golint -set_exit_status

static-app-code-scanner:
  stage: test
  extends:
    - .golang-image
  allow_failure: true
  before_script:
    - curl -sfL https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s -- -b $GOPATH/bin v2.3.0
  script:
    - cd src
    - gosec ./...

build-app:
  stage: build
  image: docker:19.03.8
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    FULL_IMAGE: gcr.io/${GOOGLE_PROJECT_ID}/hello-world
  services:
    - docker:19.03.8-dind
  before_script:
    - mkdir -p keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - cat ${CI_PROJECT_DIR}/keys/key-file.json | docker login -u _json_key --password-stdin https://gcr.io
  script:
    - docker build -t ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA} .
    - docker push ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - export IMAGE_ID=$(docker image inspect ${FULL_IMAGE}:${CI_COMMIT_SHORT_SHA} --format '{{index .RepoDigests 0}}')
    - |
        cat > ${IMAGE_DETAILS} <<EOL
        image: ${FULL_IMAGE} digest: ${IMAGE_ID} TAG: ${CI_COMMIT_SHORT_SHA}
        EOL
    - cat image-details.txt
  artifacts:
    expire_in: 24 hours
    paths:
      - image-details.txt

container-structure:
  stage: security-review
  image: docker:19.03.8
  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    FULL_IMAGE: gcr.io/${GOOGLE_PROJECT_ID}/hello-world
  services:
    - docker:19.03.8-dind
  allow_failure: true
  before_script:
    - wget -O container-structure-test-linux-amd64 https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64
    - chmod +x container-structure-test-linux-amd64
    - mv container-structure-test-linux-amd64 container-structure-test
    - mkdir -p keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - cat ${CI_PROJECT_DIR}/keys/key-file.json | docker login -u _json_key --password-stdin https://gcr.io
  script:
    - export IMAGE="$(cat image-details.txt | awk '{print $2 }')"
    - export TAG="$(cat image-details.txt | awk '{print $6 }')"
    - docker pull ${IMAGE}:${TAG}
    - ./container-structure-test test --image "${IMAGE}:${TAG}" --config security/container-structure-policy.yaml

security-acceptance:
  stage: security-review
  allow_failure: false
  when: manual
  variables:
    ACTOR: "security"
  extends:
    - .create-attestation
  artifacts:
    expire_in: 24 hours
    paths:
      - ${CI_PROJECT_DIR}/tmp/generated_payload.json

# Scan for CVEs
image-scan-vulnerabilities:
  stage: security-review
  image:
    name: "${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/kritis-signer"
    # entrypoint: ["/bin/bash"]
  script:
    - cat image-details.txt
    - export IMAGE_DIGEST="$(cat image-details.txt | awk '{print $4 }')"
    - mkdir -p keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - export GOOGLE_APPLICATION_CREDENTIALS=${CI_PROJECT_DIR}/keys/key-file.json
    # NOTE: Signer does NOT fail if image is not present
    - |
      /signer \
      -v=10 \
      -alsologtostderr \
      -image="${IMAGE_DIGEST}" \
      -policy=security/container-analysis-policy.yaml \
      -vulnz_timeout=1m \
      -mode=check-only || error=true
    - if [[ $error == true ]]; then echo "Container Analysis failed due to CVE thresholds being triggered"; exit 1; fi

deploy-development:
  stage: development
  image:
    name: gcr.io/cloud-builders/gke-deploy:stable
    entrypoint: [""]
  environment:
    name: development
    url: http://dev.secure.ensor-labs.com
  variables:
    ACTOR: "security"
  before_script:
    - mkdir -p keys
    - mkdir -p tmp
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud auth activate-service-account cicd-builds@${GOOGLE_PROJECT_ID}.iam.gserviceaccount.com --key-file=${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud --quiet config set project ${GOOGLE_PROJECT_ID}
    - export IMAGE_DIGEST="$(cat image-details.txt | awk '{print $4 }')"
    - export CLUSTER_NAME="bin-auth-dev" # hardcoded in Terraform #TODO: Create and pull from secret during creation
  script:

    # Verify attestations exist for image+digest
    - |
      export ATTESTATION_LIST=$(gcloud container binauthz attestations list \
      --project="${GOOGLE_PROJECT_ID}" \
      --attestor="projects/${GOOGLE_PROJECT_ID}/attestors/${ACTOR}-attestor" \
      --filter="resourceUri=https://${IMAGE_DIGEST}" --format="value('name')")

    # Check if attestation created for security-attestor (Note: This is NOT necessary because the Policy rejects non-attestation builds)
    - if [ ! -z "$ATTESTATION_LIST" ]; then echo "Required Attestation for '${ACTOR}-attestor' does not exist" && exit 1; fi

    - gcloud container clusters get-credentials ${CLUSTER_NAME} --zone us-central1-a
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
    # Build manifest files
    - cd ${CI_PROJECT_DIR}/k8s/overlays/development
    - ${CI_PROJECT_DIR}/kustomize edit set image busybox="${IMAGE_DIGEST}"
    - ${CI_PROJECT_DIR}/kustomize build > ${CI_PROJECT_DIR}/k8s/overlays/development/deploy-manifest.yaml
    - kubectl apply -f deploy-manifest.yaml
    # Look for Events=FailedCreate and the Image+Digest
    - sleep 3s # wait for deployment to sync
    - export ERRORS=$(kubectl get event --field-selector reason=FailedCreate | grep "${IMAGE_DIGEST}")
    - if [ ! -z "$ERRORS" ]; then echo "Failed Deployment, rolling back" && echo "${ERRORS}" && kubectl delete -f deploy-manifest.yaml && exit 1; fi
  artifacts:
    expire_in: 1 day
    paths:
      - ${CI_PROJECT_DIR}/k8s/overlays/development/deploy-manifest.yaml
    when: always

accept-development:
  stage: development
  when: manual
  allow_failure: false
  variables:
    ACTOR: "build"
  extends:
    - .create-attestation
  only:
    refs:
      - master

#### Required Attestation is "security" and "build"
deploy-QA:
  image:
    name: gcr.io/cloud-builders/gke-deploy:stable
    entrypoint: [""]
  stage: QA
  when: on_success
  environment:
    name: qa
    url: http://qa.secure.ensor-labs.com
  before_script:
    - mkdir -p keys
    - mkdir -p tmp
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud auth activate-service-account cicd-builds@${GOOGLE_PROJECT_ID}.iam.gserviceaccount.com --key-file=${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud --quiet config set project ${GOOGLE_PROJECT_ID}
    - export IMAGE_DIGEST="$(cat image-details.txt | awk '{print $4 }')"
    - export CLUSTER_NAME="bin-auth-qa" # hardcoded in Terraform #TODO: Create and pull from secret during creation
  script:
    - gcloud container clusters get-credentials ${CLUSTER_NAME} --zone us-central1-a
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
    # Build manifest files
    - cd ${CI_PROJECT_DIR}/k8s/overlays/qa
    - ${CI_PROJECT_DIR}/kustomize edit set image busybox="${IMAGE_DIGEST}"
    - ${CI_PROJECT_DIR}/kustomize build > ${CI_PROJECT_DIR}/k8s/overlays/qa/deploy-manifest.yaml
    - kubectl apply -f deploy-manifest.yaml
    # Look for Events=FailedCreate and the Image+Digest
    - sleep 3s
    - export ERRORS=$(kubectl get event --field-selector reason=FailedCreate | grep "${IMAGE_DIGEST}")
    - if [ ! -z "$ERRORS" ]; then echo "Failed Deployment" && echo "${ERRORS}" && exit 1; fi
  only:
    refs:
      - master

accept-QA:
  stage: QA
  when: manual
  allow_failure: false
  variables:
    ACTOR: "quality"
  extends:
    - .create-attestation
  only:
    refs:
      - master

deploy-production:
  stage: prod
  image:
    name: gcr.io/cloud-builders/gke-deploy:stable
    entrypoint: [""]
  when: on_success
  environment:
    name: prod
    url: http://prod.secure.ensor-labs.com
  before_script:
    - mkdir -p keys
    - mkdir -p tmp
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud auth activate-service-account cicd-builds@${GOOGLE_PROJECT_ID}.iam.gserviceaccount.com --key-file=${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud --quiet config set project ${GOOGLE_PROJECT_ID}
    - export IMAGE_DIGEST="$(cat image-details.txt | awk '{print $4 }')"
    - export CLUSTER_NAME="bin-auth-prod" # hardcoded in Terraform #TODO: Create and pull from secret during creation
  script:
    - gcloud container clusters get-credentials ${CLUSTER_NAME} --zone us-central1-a
    - curl -s "https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh" | bash
    # Build manifest files
    - cd ${CI_PROJECT_DIR}/k8s/overlays/production
    - ${CI_PROJECT_DIR}/kustomize edit set image busybox="${IMAGE_DIGEST}"
    - ${CI_PROJECT_DIR}/kustomize build > ${CI_PROJECT_DIR}/k8s/overlays/production/deploy-manifest.yaml
    - kubectl apply -f deploy-manifest.yaml
    # Look for Events=FailedCreate and the Image+Digest
    - sleep 3s
    - export ERRORS=$(kubectl get event --field-selector reason=FailedCreate | grep "${IMAGE_DIGEST}")
    - if [ ! -z "$ERRORS" ]; then echo "Failed Deployment" && echo "${ERRORS}" && exit 1; fi
  only:
    refs:
      - master



################################### Infrastructure
.terraform-base:
  image:
    name: hashicorp/terraform:$TERRAFORM_IMAGE_VERSION
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - cd ${TF_ROOT}
    - rm -rf .terraform/ || true
    - mkdir -p ${CI_PROJECT_DIR}/keys
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - FILE=${CI_PROJECT_DIR}/keys/key-file.json
    - if [ ! -f "$FILE" ]; then echo "Key file does not exist, exiting" && exit 1; fi
    - terraform init -backend-config="credentials=${FILE}"

setup-infrastructure:
  stage: infrastructure-setup
  extends:
  - .terraform-base
  script:
  - terraform plan
  only:
    refs:
      - tags

plan-infrastructure:
  stage: infrastructure-plan
  extends:
  - .terraform-base
  script:
  - terraform plan
  only:
    refs:
      - tags

apply-infrastructure:
  stage: infrastructure-apply
  extends:
  - .terraform-base
  script:
  - terraform plan
  only:
    refs:
      - tags

###################################################################
###################################################################
# Private JOB to inherit common golang image
.golang-image:
  image: golang:1.13

# Creates an attestation based on the ACTOR variable
.create-attestation:
  image: google/cloud-sdk:debian_component_based
  variables:
    ACTOR: "OVERRIDEME"
  before_script:
    - mkdir -p keys
    - mkdir -p tmp
    - echo $GOOGLE_BUILD_GSA | base64 -d > ${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud auth activate-service-account cicd-builds@${GOOGLE_PROJECT_ID}.iam.gserviceaccount.com --key-file=${CI_PROJECT_DIR}/keys/key-file.json
    - gcloud --quiet config set project ${GOOGLE_PROJECT_ID}
  script:
    - export KEYRING_NAME="$(gcloud secrets versions access latest --secret='keyring-name')"
    # kms keyrings list produces a long string including the project and location, the below command only uses the keyring name
    - export KEYRING_NAME="${KEYRING_NAME##*/}"
    # Extract Image Path and Image Digest from generated file during image creation
    - export IMAGE_DIGEST="$(cat image-details.txt | awk '{print $4 }')"
    # - gcloud kms keys versions get-public-key key-version --location us-central1  --keyring ${KEYRING_NAME} --key ${ACTOR}-attestor-key --output-file ./output.pub
    - export PUBLIC_KEY_ID=$(gcloud container binauthz attestors describe ${ACTOR}-attestor --format='value(userOwnedGrafeasNote.publicKeys[0].id)')
    # Create the file to sign
    - gcloud container binauthz create-signature-payload --artifact-url=${IMAGE_DIGEST} > ${CI_PROJECT_DIR}/tmp/generated_payload.json
    # Sign generated_payload.json with KMS
    - |
        gcloud kms asymmetric-sign \
        --location us-central1 \
        --keyring ${KEYRING_NAME} \
        --key ${ACTOR}-attestor-key \
        --version 1 \
        --digest-algorithm sha512 \
        --input-file ${CI_PROJECT_DIR}/tmp/generated_payload.json \
        --signature-file ${CI_PROJECT_DIR}/tmp/ec_signature
    # Create attestation
    - |
      gcloud container binauthz attestations create \
        --artifact-url="${IMAGE_DIGEST}" \
        --attestor="projects/${GOOGLE_PROJECT_ID}/attestors/${ACTOR}-attestor" \
        --signature-file=${CI_PROJECT_DIR}/tmp/ec_signature \
        --public-key-id="${PUBLIC_KEY_ID}"
